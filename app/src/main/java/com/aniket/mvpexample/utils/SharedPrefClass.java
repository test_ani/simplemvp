package com.aniket.mvpexample.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/***
 * class to save and retrieve data in the preferences
 *
 * @author Aniket
 */
public class SharedPrefClass {

    /* private members */
    @SuppressWarnings("unused")
    private Context context;

    private SharedPreferences preferences = null;
    private static SharedPrefClass sharedPreferenceDataManager = null;

    private static final String PREF_NAME = "AppPref";

    private static final String LOGIN_STATUS = "LoginStatus";
    private static final String FACEBOOK_LOGIN = "FacebookLogin";
    private static final String TWITTER_LOGIN = "TwitterLogin";

    // If PIN is selected/created for Authentication

    private static final String GIVEN_NAME = "given_name";
    private static final String FAMILY_NAME = "family_name";
    private static final String BIRTH_DATE = "birthdate";
    private static final String EMAIL = "email";
    private static final String PHONE_NUMBER = "phone_number";

    private static final String FCM_TOKEN = "FCMToken";
    private static final String FCM_TOKEN_UPDATED_ON_SERVER = "FCMTokenUpdatedOnServer";

    /***
     * this is constructor for saving and retrieving the values from shared_preferences.
     *
     * @param context
     */
    public SharedPrefClass(Context context) {
        this.context = context;
        if (preferences == null) {
//            preferences = PreferenceManager.getDefaultSharedPreferences(context);
            preferences = context.getSharedPreferences(PREF_NAME, 0);
        }
    }

    /***
     * this is static function to create singleton object.
     *
     * @param context
     * @return
     */
    public static SharedPrefClass getInstance(Context context) {
        if (sharedPreferenceDataManager == null) {
            sharedPreferenceDataManager = new SharedPrefClass(context);
        }
        return sharedPreferenceDataManager;
    }

    /***
     * this is function to clear preferences.
     *
     * @return
     */
    public void clearSharedPrefs() {
        Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    /***
     * this will return saved preference having value as string
     *
     * @param key
     * @return
     */
    public String getSavedStringPreference(String key) {
        String value = preferences.getString(key, null);
        return value;
    }

    /***
     * this will return saved preference having value as boolean
     *
     * @param key
     * @return
     */
    public boolean getSavedBooleanPreference(String key) {
        boolean value = preferences.getBoolean(key, false);
        return value;
    }

    /***
     * this will return saved preference having value as boolean
     *
     * @param key
     * @return
     */
    public long getSavedLongPreference(String key) {
        long value = preferences.getLong(key, 0);
        return value;
    }

    public float getSavedFloatPreference(String key) {
        float value = preferences.getFloat(key, 0f);
        return value;
    }

    /***
     * @param key
     * @param value
     */
    public void savePreference(String key, String value) {
        Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }


    /***
     * @param key
     * @param value
     */
    public void savePreference(String key, long value) {
        Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    /***
     * @param key
     * @param value
     */
    public void savePreference(String key, float value) {
        Editor editor = preferences.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    /***
     * @param key
     * @param value
     */
    public void savePreference(String key, boolean value) {
        Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    /***
     * this will return saved preference having value as boolean
     *
     * @param key
     * @return
     */
    public boolean getSavedBooleanPreferenceWithDefaultValue(String key, boolean returnDefaultValue) {
        boolean value = preferences.getBoolean(key, returnDefaultValue);
        return value;
    }

    /**
     * @param key
     * @param array
     * @return
     */
    public void savePreference(String key, boolean[] array) {
        Editor editor = preferences.edit();
        editor.putInt(key + "_size", array.length);
        for (int i = 0; i < array.length; i++)
            editor.putBoolean(key + "_" + i, array[i]);
        editor.apply();
    }

    public boolean[] getSavedBooleanArray(String key) {
        int size = preferences.getInt(key + "_size", 0);
        boolean array[] = new boolean[size];
        for (int i = 0; i < size; i++)
            array[i] = preferences.getBoolean(key + "_" + i, false);
        return array;
    }

    /* check login or not */
    public boolean isUserLogin() {
        return preferences.getBoolean(LOGIN_STATUS, false);
    }

    public void setUserLogin(boolean loginStatus) {
        Editor editor = preferences.edit();
        editor.putBoolean(LOGIN_STATUS, loginStatus);
        editor.apply();
    }

    public boolean isFacebookLogin() {
        return preferences.getBoolean(FACEBOOK_LOGIN, false);
    }

    public void setFacebookLogin(boolean facebookLogin) {
        Editor editor = preferences.edit();
        editor.putBoolean(FACEBOOK_LOGIN, facebookLogin);
        editor.apply();
    }

    public boolean isTwitterLogin() {
        return preferences.getBoolean(TWITTER_LOGIN, false);
    }

    public void setTwitterLogin(boolean twitterLogin) {
        Editor editor = preferences.edit();
        editor.putBoolean(TWITTER_LOGIN, twitterLogin);
        editor.apply();
    }


    public String getUserFirstName() {
        return preferences.getString(GIVEN_NAME, "");
    }

    public void setUserFirstName(String firstName) {
        Editor editor = preferences.edit();
        editor.putString(GIVEN_NAME, firstName);
        editor.apply();
    }

    public String getUserLastName() {
        return preferences.getString(FAMILY_NAME, "");
    }

    public void setUserLastName(String lastName) {
        Editor editor = preferences.edit();
        editor.putString(FAMILY_NAME, lastName);
        editor.apply();
    }

    public String getUserDOB() {
        return preferences.getString(BIRTH_DATE, "");
    }

    public void setUserDOB(String dateOfBirth) {
        Editor editor = preferences.edit();
        editor.putString(BIRTH_DATE, dateOfBirth);
        editor.apply();
    }

    public String getUserEmail() {
        return preferences.getString(EMAIL, "");
    }

    public void setUserEmail(String email) {
        Editor editor = preferences.edit();
        editor.putString(EMAIL, email);
        editor.apply();
    }

    public String getUserPhoneNumber() {
        return preferences.getString(PHONE_NUMBER, "");
    }

    public void setUserPhoneNumber(String mobile) {
        Editor editor = preferences.edit();
        editor.putString(PHONE_NUMBER, mobile);
        editor.apply();
    }


    public String getFCMToken() {
        return preferences.getString(FCM_TOKEN, "");
    }

    public void setFCMToken(String FCMToken) {
        Editor editor = preferences.edit();
        editor.putString(FCM_TOKEN, FCMToken);
        editor.apply();
    }

    public boolean isFCMTokenUpdatedOnServer() {
        return preferences.getBoolean(FCM_TOKEN_UPDATED_ON_SERVER, false);
    }

    public void setFCMTokenUpdatedOnServer(boolean isUpdatedOnServer) {
        Editor editor = preferences.edit();
        editor.putBoolean(FCM_TOKEN_UPDATED_ON_SERVER, isUpdatedOnServer);
        editor.apply();
    }

    public void clearPrefs() {
        Editor editor = preferences.edit();
        editor.putBoolean(LOGIN_STATUS, false);
        editor.putBoolean(FACEBOOK_LOGIN, false);
        editor.putBoolean(TWITTER_LOGIN, false);
        editor.putString(GIVEN_NAME, "");
        editor.putString(FAMILY_NAME, "");
        editor.putString(EMAIL, "");
        editor.putString(BIRTH_DATE, "");
        editor.putString(PHONE_NUMBER, "");
        editor.apply();
    }
}
