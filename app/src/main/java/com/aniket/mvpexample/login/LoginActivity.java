package com.aniket.mvpexample.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.aniket.mvpexample.MainActivity;
import com.aniket.mvpexample.R;

public class LoginActivity extends AppCompatActivity implements LoginView {

    private EditText usernameEditText;
    private EditText passwordEditText;
    private Button loginButton;
    private ProgressBar loadProgressBar;

    private LoginPresenter mLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mLoginPresenter = new LoginPresenterImpl(this);

        usernameEditText = findViewById(R.id.ed_username);
        passwordEditText = findViewById(R.id.ed_password);
        loginButton = findViewById(R.id.btn_login);
        loadProgressBar = findViewById(R.id.pb_load);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginPresenter.validateCredentials(usernameEditText.getText().toString().trim(),
                        passwordEditText.getText().toString().trim());
            }
        });
    }

    @Override
    public void showProgress() {
        loadProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        loadProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void setUsernameError() {
        usernameEditText.setError("Username empty");
    }

    @Override
    public void setPasswordError() {
        passwordEditText.setError("Password empty");
    }

    @Override
    public void navigateToMain() {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
    }

    @Override
    public void showAlert(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLoginPresenter.onDestroy();
    }
}
