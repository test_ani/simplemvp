package com.aniket.mvpexample.login;

import android.text.TextUtils;

public class LoginInteractorImpl implements LoginInteractor {
    @Override
    public void login(String username, String password, OnLoginFinishedListener listener) {
        if (TextUtils.isEmpty(username)) {
            listener.onUsernameError();
        } else if (TextUtils.isEmpty(password)) {
            listener.onPasswordError();
        } else if (username.equals("admin") && password.equals("12345")) {
            listener.onSuccess();
        } else {
            listener.onFailure("Invalid Credentials");
        }
    }
}
