package com.aniket.mvpexample.login;

public interface LoginInteractor {
    interface OnLoginFinishedListener {
        void onUsernameError();

        void onPasswordError();

        void onSuccess();

        void onFailure(String message);
    }

    void login(String username, String password, OnLoginFinishedListener listener);
}
