package com.aniket.mvpexample.network;

import com.android.volley.VolleyError;

import org.json.JSONArray;

/**
 * Created by aniket on 4/7/2017.
 */
public interface VolleyJSONArrayCallback {
    void volleyJSONArrayCallbackSuccess(JSONArray response, String requestTag);

    void volleyJSONArrayCallbackError(VolleyError error, String requestTag);
}
