package com.aniket.mvpexample.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class NetworkUtils {
    private static final String TAG = NetworkUtils.class.getSimpleName();
    private static final int REQUEST_TIMEOUT = 30000;
    private Context mContext;

    public NetworkUtils(Context mContext) {
        this.mContext = mContext;
    }

    public void callVolleyStringRequest(int method, String url,
                                        final Map<String, String> params, final String requestTag,
                                        final VolleyStringRequestCallback callback) {
        final String deviceLocale = Locale.getDefault().getLanguage();
        Log.e(TAG, deviceLocale);

        StringRequest strReq = new StringRequest(method, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "Success " + response);
                        callback.volleyStringRequestCallbackSuccess(response, requestTag);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "ERROR " + error.toString());
                        callback.volleyStringRequestCallbackError(error, requestTag);
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(REQUEST_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        VolleyHelper.getInstance(mContext).addToRequestQueue(strReq, requestTag);
    }

    // Call Volley JSONObject
    public void callVolleyJSONObjectRequest(int method, String url,
                                            JSONObject jsonParams, final String requestTag,
                                            final VolleyJSONObjectCallback callback) {
        final String deviceLocale = Locale.getDefault().getLanguage();
        Log.e(TAG, deviceLocale);

        JsonObjectRequest jsonReq = new JsonObjectRequest(method, url, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "Success " + response);
                        callback.volleyJSONObjectCallbackSuccess(response, requestTag);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "ERROR " + error.toString());
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//                            Toast.makeText(mContext,
//                                    mContext.getString(R.string.error_network_timeout),
//                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            //TODO
                        } else if (error instanceof ServerError) {
                            //TODO
                        } else if (error instanceof NetworkError) {
                            //TODO
                        } else if (error instanceof ParseError) {
                            //TODO
                        }
                        callback.volleyJSONObjectCallbackError(error, requestTag);
                    }
                }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };

        jsonReq.setRetryPolicy(new DefaultRetryPolicy(REQUEST_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        VolleyHelper.getInstance(mContext).addToRequestQueue(jsonReq, requestTag);
    }


    // Call Volley JsonArray Request
    public void callVolleyJsonArrayRequestRequest(int method, String url,
                                                  JSONArray jsonArrayParams, final String requestTag,
                                                  final VolleyJSONArrayCallback callback) {
        final String deviceLocale = Locale.getDefault().getLanguage();
        Log.e(TAG, deviceLocale);

        JsonArrayRequest jsonArrayReq = new JsonArrayRequest(method, url, jsonArrayParams,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e(TAG, "Success " + response);
                        callback.volleyJSONArrayCallbackSuccess(response, requestTag);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "ERROR " + error.toString());
                        callback.volleyJSONArrayCallbackError(error, requestTag);
                    }
                });

        // Adding request to request queue
        VolleyHelper.getInstance(mContext).addToRequestQueue(jsonArrayReq, requestTag);
    }

    public void callVolleyStringRequestGet(int method, String url, final String requestTag,
                                           final VolleyStringRequestCallback callback) {
        final String deviceLocale = Locale.getDefault().getLanguage();
        Log.e(TAG, deviceLocale);

        StringRequest strReq = new StringRequest(method, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "Success " + response);
                        callback.volleyStringRequestCallbackSuccess(response, requestTag);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "ERROR " + error.toString());
                        callback.volleyStringRequestCallbackError(error, requestTag);
                    }
                });

        strReq.setRetryPolicy(new DefaultRetryPolicy(REQUEST_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        VolleyHelper.getInstance(mContext).addToRequestQueue(strReq, requestTag);
    }
}
