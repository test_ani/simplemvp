package com.aniket.mvpexample.network;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by aniket on 4/7/2017.
 */
public interface VolleyStringRequestCallback {
    void volleyStringRequestCallbackSuccess(String success, String requestTag);

    void volleyStringRequestCallbackError(VolleyError volleyError, String requestTag);
}
