package com.aniket.mvpexample.network;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by aniket on 4/7/2017.
 */
public interface VolleyJSONObjectCallback {
    void volleyJSONObjectCallbackSuccess(JSONObject success, String requestTag);

    void volleyJSONObjectCallbackError(VolleyError volleyError, String requestTag);
}
