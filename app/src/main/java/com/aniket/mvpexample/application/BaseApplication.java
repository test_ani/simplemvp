package com.aniket.mvpexample.application;

import android.app.Application;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class BaseApplication extends Application {

    public static final String TAG = BaseApplication.class.getSimpleName();

    public static Typeface robotoRegularTypeface;
    public static Typeface robotoThinItalicTypeface;

    @Override
    public void onCreate() {
        super.onCreate();
//        ACRA.init(this);
        robotoRegularTypeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto_Regular.ttf");
        robotoThinItalicTypeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-ThinItalic.ttf");
//        // Initialize application
//        AppHelper.init(getApplicationContext());
//        findCurrent();
    }

    public static void applyFonts(View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    applyFonts(child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(robotoRegularTypeface, Typeface.NORMAL);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void applyFontThinItalic(View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    applyFonts(child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(robotoThinItalicTypeface, Typeface.NORMAL);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}